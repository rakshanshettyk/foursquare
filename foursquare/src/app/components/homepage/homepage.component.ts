import { Component, OnInit } from '@angular/core';

import { FoodService } from '../../services/food.service';
import { Food, Venue } from '../../models/food';
import { Foodvenue } from '../../models/venuedata';
import { NavbarComponent } from '../navbar/navbar.component';
import { Foodtips } from '../../models/foodtips';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  vid: any;
  lat: number;
  lng: number;
  venueList: Venue[];

  constructor(private foodservice: FoodService) { }

  loadItems(i: number, id: any) {
    this.vid = id;
    this.foodservice.getVenueData(this.vid).subscribe(
      (data: Foodvenue) => {
        this.venueList[i].url = data.response.venue.bestPhoto.prefix + '300x500' + data.response.venue.bestPhoto.suffix;
        this.venueList[i].rating = data.response.venue.rating;
      });
      this.foodservice.getUserReviews(this.vid).subscribe(
        (data: Foodtips) => {
          this.venueList[i].topTip = data.response.tips.items[0].text;
          this.venueList[i].topTip = this.venueList[i].topTip.substring(0, 43).concat('...');
        });
  }

  ngOnInit() {
    this.foodservice.getFood()
      .subscribe((data: Food) => {
        this.venueList = data.response.venues;
        let i = 0;
        while (this.venueList[i] != null) {
          this.loadItems(i, this.venueList[i].id);
          i++;
        }
      });
      this.lat = this.foodservice.latitude;
      this.lng = this.foodservice.longitude;
  }

}
