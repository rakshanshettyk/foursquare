import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsRoutingModule } from './details-routing.module';

import { AgmCoreModule } from '@agm/core';
import { SwiperComponent } from './swiper/swiper.component';
import { HotelDetailsComponent } from './hotel-details/hotel-details.component';

@NgModule({
  declarations: [SwiperComponent, HotelDetailsComponent],
  imports: [
    CommonModule,
    DetailsRoutingModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyBZsVU8qm64-AxXNiV9h8kQnnC50Dha3Dg'})
  ],
  exports: [SwiperComponent, HotelDetailsComponent]
})
export class DetailsModule { }
