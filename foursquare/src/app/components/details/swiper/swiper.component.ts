import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { FoodService } from '../../../services/food.service';
import { Foodphotos, ItemElement } from '../../../models/foodphotos';

@Component({
  selector: 'app-swiper',
  templateUrl: './swiper.component.html',
  styleUrls: ['./swiper.component.scss']
})
export class SwiperComponent implements OnInit {

  venueId: any;
  venuePhotos: Foodphotos;
  foodArray: any[] = [];
  item: ItemElement[];

  constructor(private route: ActivatedRoute, private food: FoodService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => this.venueId = params.venueId);
    this.food.getRestaurantPhotos(this.venueId).subscribe(
      (data: Foodphotos) => {
        this.venuePhotos = data;
        let i = 0;
        this.item = this.venuePhotos.response.photos.items;
        while (this.item[i] != null) {
          this.foodArray.push(this.item[i].prefix + '300x500' + this.item[i].suffix);
          i++;
        }
        console.log(this.foodArray)
      });
  }

}
