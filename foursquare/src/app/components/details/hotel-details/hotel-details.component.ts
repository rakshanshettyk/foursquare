import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { FoodService } from '../../../services/food.service';

@Component({
  selector: 'app-hotel-details',
  templateUrl: './hotel-details.component.html',
  styleUrls: ['./hotel-details.component.scss']
})
export class HotelDetailsComponent implements OnInit {

  lat: number;
  lng: number;
  venueId: any;
  venueData: any;
  userReviews: any;

  constructor(private route: ActivatedRoute, private foodservice: FoodService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => this.venueId = params.venueId);
    this.foodservice.getVenueData(this.venueId).subscribe(
      data => this.venueData = data
    );
    this.foodservice.getUserReviews(this.venueId).subscribe(
      data => this.userReviews = data);

    this.lat = this.foodservice.latitude;
    this.lng = this.foodservice.longitude;
  }

}
