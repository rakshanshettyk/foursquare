// Generated by https://quicktype.io
export class Foodphotos {
    response: Response;
}

export class Response {
    photos: Photos;
}

export class Photos {
    count: number;
    items: ItemElement[];
}

export class ItemElement {
    id: string;
    prefix: string;
    suffix: string;
}
