// Generated by https://quicktype.io
export class Food {
    response: Response;
  }
  
  export class Response {
    venues: Venue[];
  }
  
  export class Venue {
    id: string;
    name: string;
    location: Location;
    categories: Category[];
    url: string;
    rating: number;
    topTip: string;
  }
  
  export class Category {
    id: string;
    name: string;
    pluralName: string;
    shortName: string;
  }
  
  export class Location {
    address?: string;
    lat: number;
    lng: number;
    labeledLatLngs: LabeledLatLng[];
    distance: number;
    postalCode?: string;
    city?: string;
    state: string;
    formattedAddress: string[];
    crossStreet?: string;
  }
  export class LabeledLatLng {
    lat: number;
    lng: number;
  }  